const express = require('express')
const app = express()
const user = require('./user')

app.get('/', (req, res) => {
  res.json({
    msg: 'hello from bitbucket pipelines! - edited'
  })
})

app.get('/name', (req, res) => {
  res.json({
    msg: user.generateUser()
  })
})

const port = 5000
app.listen(port, () => console.log(`App running on localhost:${port}`))